# Maintainer: Caleb Connolly <caleb@connolly.tech>
# Kernel config based on: arch/arm64/configs/defconfig

_flavor="oneplus-sdm845"
pkgname=linux-$_flavor
pkgver=5.7_rc6
pkgrel=1
pkgdesc="Kernel fork for OnePlus SDM845 devices, close to mainline"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/sdm845-mainline/sdm845-linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="bison findutils flex installkernel openssl-dev perl"

_config="config-$_flavor.$arch"
_commit="d99aa17c6fd4528f0fc375b86d3390562046ba88"

# Source
source="
	sdm845-linux-$_commit.tar.gz::https://gitlab.com/sdm845-mainline/sdm845-linux/-/archive/$_commit/sdm845-linux-$_commit-sdm845.tar.gz
	$_config
"
builddir="$srcdir/sdm845-linux-$_commit"

prepare() {
	default_prepare
	mkdir -p "$srcdir"/build
	cp -v "$srcdir"/$_config "$srcdir"/build/.config
	make -C "$builddir" O="$srcdir"/build ARCH="$_carch" \
		olddefconfig
}

build() {
	cd "$srcdir"/build
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	cd "$srcdir/build/arch/$_carch/boot"

	install -D "$srcdir/build/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	cd "$srcdir"/build
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}

sha512sums="cc533f97d5bf6a42f50c3487bb44286b6ebc594fe37b0818575bef6ef70a8d55ee630ba9c1534bf39159a7ec88ba7fccef4457dca2864268f6ca38400e6857a7  sdm845-linux-d99aa17c6fd4528f0fc375b86d3390562046ba88.tar.gz
02b825b64f592f56098ee4de62f1692b63d74f4f7bbd2592ec7e105b308b3a241c9a5d132fe5cfd49002bbfd3dfcba9181ea451e680713e83d64796c8c9aa308  config-oneplus-sdm845.aarch64"
